using NUnit.Framework;
using ProvaPratica;
namespace prova_pratica.Test
{
    public class Tests
    {
        [Test]
        public void Cpf()
        {
            string cpf = "05550367076";
            Assert.AreEqual(true, Validador.CPF(cpf));
        }
        [Test]
        public void Cnpj()
        {
            string cpf = "08881165000121";
            Assert.AreEqual(true, Validador.CNPJ(cpf));
        }
    }
}